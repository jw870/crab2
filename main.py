import numpy as np
import glob
import midi
import copy
import matplotlib.pyplot as plt
import sklearn.model_selection
import keras

#from quiver_engine import server

from keras.optimizers import SGD, adam, rmsprop
from keras.callbacks import EarlyStopping
from keras.utils import np_utils

#from vis.losses import ActivationMaximization

#from data import load_data
import model as _model

#def check_matrix(mat):
#    # Checks for matrices that have no volume info
#    nonZeroEntries = mat[mat > 0]
#    print np.std(mat), np.mean(mat)

def accumulate_ticks(track):
    tickList = []
    cumulativeTicks = 0
    for event in track:
        cumulativeTicks += event.tick
        tickList.append(cumulativeTicks)
    return tickList

def plot_sample(sample, segPred=None, sampleSize=200):
    if len(sample.shape) == 3:
        sample = sample[:,:,0]
    fig = plt.figure()
    if segPred is None:
        ax = fig.add_subplot('111')
    else:
        ax = fig.add_subplot('211')
        axPred = fig.add_subplot('212', sharex=ax)
        xPred = sampleSize*(0.5 + np.array(range(len(segPred))))
        #for iC in xrange(segPred.shape[1]):
        axPred.plot(xPred, segPred[:,0])
        axPred.axhline(0.5, ls='dotted', c='k')
        print xPred
    ax.matshow(sample.T, origin='lower')
    ax.set_xlabel('Time')
    ax.set_ylabel('Pitch')
    plt.show()

def load_midi_file(fname, debug=False, dt=0.05):
    # dt: time resolution of the returned pitch-time matrix
    pattern = midi.read_midifile(fname)

    # NB event[i].tick is the time elapsed between event[i-1] and event[i], i.e. the tick
    #   is the length of time BEFORE an event, not AFTER it.
    tickLists = map(accumulate_ticks, pattern)

    # Get an ordered list of events across all tracks
    orderedEventInd = []
    for iTrack, tickList in enumerate(tickLists):
        for iEvent, tick in enumerate(tickList):
            orderedEventInd.append((tick, iTrack, iEvent))
    orderedEventInd = sorted(orderedEventInd)

    # Tempo track
    tempo = pattern[0]

    # Instrumental tracks
    insts = pattern[1:]

    # Loop over all events
    lastTick=0
    allNotes = []
    noteTicks = []
    noteTimes = []
    activeNotes = {}
    timeSoFar = 0.0

    
    for i, (tick, iTrack, iEvent) in enumerate(orderedEventInd):
        event = pattern[iTrack][iEvent]

        lastEventTick = orderedEventInd[i-1][0]
        lastEventTime = timeSoFar
        if i>0:
            ticksFromLastEvent = tick - lastEventTick
        else:
            ticksFromLastEvent = 0
        if ticksFromLastEvent > 0:
            timeSoFar += secondsPerTick*ticksFromLastEvent

        # If this event's tick differs from the last one, then we have advanced in time
        #   and should store the previous activeNotes
        #if event.tick != 0:
        #if tick != lastTick and event.name == 'Note On' or event.name == 'Note Off':
        #print 'event:', event, tick, iTrack
        #if tick != lastTick:
        if ticksFromLastEvent > 0:
            lastTick = tick
            #print '!', tick, lastTick
            # TODO check whether actually different from previous activeNotes...
            allNotes.append(copy.deepcopy(activeNotes))
            noteTicks.append(lastEventTick)
            noteTimes.append(lastEventTime)
            #print 'Stored new notes', activeNotes, tick, timeSoFar


        if event.name == 'Set Tempo':
            #msPerBeat = event.data[0]*256**2 + event.data[1]*256 + evnet[2]
            beatsPerMinute = event.get_bpm()
            microSecondsPerBeat = (60.*1e6)/beatsPerMinute

            # The resolution is the number of pulses per quarter note
            resolution = pattern.resolution

            secondsPerTick = 1e-6*microSecondsPerBeat/resolution



        if event.name == 'Note On' or event.name == 'Note Off':

            pitch = event.pitch
            velocity = event.velocity
            tick = event.tick

            # If the velocity is zero, this is really a note off event 
            if velocity == 0 or event.name == 'Note Off':
                #activeNotes.remove(pitch)
                if pitch in activeNotes:
                    del activeNotes[pitch]
                else:
                    #print 'WARNING:', fname, tick, iTrack, iEvent
                    pass
            else:
                activeNotes[pitch] = velocity
                #activeNotes[pitch] = 1

        else:
            #print "Can't interpret event type", event.name
            pass

        # TEST: Volumes will die off over time
        #for note in activeNotes:
        #    activeNotes[note] *= 0.99

    # Put into pitch-time matrix, for now ignoring tempo
    minPitch = midi.C_2
    #maxPitch = midi.C_9
    maxPitch = midi.Gs_8
    mPT = np.zeros((len(allNotes), maxPitch-minPitch))
    for iT, notes in enumerate(allNotes):
        for note in notes:
            relNote = note - minPitch
            mPT[iT][relNote] = notes[note]

    # Assign durations to each note
    print 'Total duration', timeSoFar
    nT = int(round(timeSoFar/dt + 1))
    mPT2 = np.zeros((nT, maxPitch-minPitch))
    #for iT, notes in enumerate(allNotes):
    #    for note in notes:
    #        relNote = note - minPitch
    #        mPT[iT][relNote] = notes[note]
    iSlice=0
    for iT in xrange(nT):
        t = dt*iT
        #if iSlice!=len(allNotes)-1 and t > noteTimes[iSlice+1]:
        if t > noteTimes[iSlice+1]:
            iSlice+=1
        for note in allNotes[iSlice]:
            relNote = note - minPitch
            mPT2[iT][relNote] = allNotes[iSlice][note]
        if iSlice == len(allNotes)-1:
            break

    if debug:
        fig = plt.figure()
        ax = fig.add_subplot('111')
        ax.matshow(mPT2.T, origin='lower')
        plt.show()

    #return mPT
    return mPT2


# Generate some training data by chopping up a midi file
def chop_midi(mat, sampleSize=100, stepFrac=4):
    iStart=0
    samples = []
    while True:
        if iStart+sampleSize >= mat.shape[0]:
            break
        sample = mat[iStart:iStart+sampleSize]
        samples.append(sample)
        iStart += sampleSize/stepFrac
    return samples

if __name__ == '__main__':
    # Load data

    composers = ['beethoven', 'chopin', 'bach']

    sampleTime = 10.0 # seconds
    dt = 0.05
    sampleSize = int(round(sampleTime/dt))
    
    readData = True
    loadModelFromDisk = False

    if readData:
        X = []
        Y = []
        YExtraData = []
        for iC, composer in enumerate(composers):
            midiFnames = glob.glob(composer+'/*.mid')
            for midiFname in midiFnames:
                #midiFname = 'chopin/chpn-p4.mid'
                mat = load_midi_file(midiFname, debug=False, dt=dt)
                #mat = load_midi_file('beethoven/elise.mid', debug=True)
                newSamples = chop_midi(mat, sampleSize=sampleSize, stepFrac=3)
                #samples.append(newSamples)
                print 'Matrix size for', midiFname, 'is', mat.shape[0]
                
                for i in xrange(len(newSamples)):
                    newSamples[i] = np.array(newSamples[i])
                    newSamples[i] = newSamples[i].reshape(newSamples[i].shape[0], 
                                                newSamples[i].shape[1], 1)
                    #X = X.reshape(X.shape[0], X.shape[1], X.shape[2], 1)

                #Y = np_utils.to_categorical(Y, len(composers))

                newSamples = np.array(newSamples)
                YExtraData.append((midiFname, mat))
                X.append(newSamples)
                Y.append([iC]*len(newSamples))
                Y[-1] = np.array(Y[-1])
                Y[-1] = Y[-1].astype(int)
                Y[-1] = np_utils.to_categorical(Y[-1], len(composers))


        nSamples = sum(map(len, X))
        trainFrac = 0.8

        # Shuffle
        XY = zip(X, Y, YExtraData)
        np.random.shuffle(XY)
        X, Y, YExtraData = zip(*XY)

        xTrain = []
        yTrain = []
        i=0
        totTrainSamples = 0
        while totTrainSamples < trainFrac*nSamples:
            #xTrain += X[i]
            #yTrain += Y[i]
            totTrainSamples += len(X[i])
            i+=1
        xTrain = X[:i]
        yTrain = Y[:i]
        xTest = X[i:]
        yTest = Y[i:]
        YExtraDataTest = YExtraData[i:]

        # Currently indices will run as xTrain[iPiece][iSample]; 
        #   want to join them into contiguous samples
        xTestByPiece = copy.deepcopy(xTest)
        yTestByPiece = copy.deepcopy(yTest)

        xTrain = np.concatenate(xTrain)
        xTest = np.concatenate(xTest)

        yTrain = np.concatenate(yTrain)
        yTest = np.concatenate(yTest)


        # One-hot encoding


    totals = np.sum(yTrain, axis=0)
    totalsDict = {}
    for i, t in enumerate(totals):
        totalsDict[composers[i]] = t
    print 'Training set data counts are:', totalsDict

    totals = np.sum(yTest, axis=0)
    totalsDict = {}
    for i, t in enumerate(totals):
        totalsDict[composers[i]] = t
    print 'Test set data counts are:', totalsDict

    batch_size = 128
    nb_epoch = 12

    # Load data
    #(X_train, y_train, X_test, y_test) = load_data()

    # Load and compile model
    reload(_model)
    model = _model.get_model(xTrain.shape[1], xTrain.shape[2], len(composers))

    earlyStoppingCallback = EarlyStopping(monitor='val_acc', verbose=1, patience=5)

    #modelSaveCallback = keras.callbacks.ModelCheckpoint('weights.{epoch:02d}-{val_loss:.2f}.hdf5',
    #                                    monitor='val_loss', verbose=1, save_best_only=True)
    mdlSaveFname = 'weights.{epoch:02d}-{val_loss:.2f}.hdf5'
    modelSaveCallback = keras.callbacks.ModelCheckpoint(mdlSaveFname,
                                        monitor='val_acc', verbose=1, save_best_only=False)


    #optimizer = adam(lr=0.002)
    optimizer = adam()
    model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                  metrics=['accuracy'])

    if loadModelFromDisk:
        model = keras.models.load_model('mdl.hdf5')

        # Read test set from disk
        f = open('test-set.txt', 'r')
        testSetFnames = list(f.readlines())
        f.close()
        YExtraDataTest = []
        for fname in testSetFnames:
            mat = load_midi_file(fname.rstrip(), debug=False, dt=dt)
            #if not check_matrix(mat):
            #    print fname, "!!!"
            YExtraDataTest.append((fname, mat))
    else:
        model.fit(xTrain, yTrain, batch_size=batch_size, nb_epoch=nb_epoch,
                  verbose=1, validation_data=(xTest, yTest), callbacks=[earlyStoppingCallback,modelSaveCallback])

        print("Loading best-performing network")
        model = keras.models.load_model('mdl.hdf5')
        score = model.evaluate(xTest, yTest, verbose=1)
        print("Accuracy:", score[1])

        # Write test set to disk
        testSetFnames = [yi[0] for yi in YExtraDataTest]
        f = open('test-set.txt', 'w')
        f.write('\n'.join(testSetFnames))
        f.close()

    # Test
    allYPred = []
    allYSeg = []

    classifications = []
    totalCorrect=0
    for iPiece, piece in enumerate(xTestByPiece):
        # Classify with no overlap between sample windows
        chopped = chop_midi(YExtraDataTest[iPiece][1], sampleSize=sampleSize, stepFrac=1)
        chopped = np.array(chopped)
        chopped = chopped.reshape(chopped.shape[0], chopped.shape[1], chopped.shape[2], 1)
        ySeg = model.predict(chopped)
        allYSeg.append(ySeg)
        yPred = np.mean(ySeg, axis=0)
        classification = composers[np.argmax(yPred)]

        allYPred.append(yPred)


        trueClassification = YExtraDataTest[iPiece][0].split('/')[0]

        if classification != trueClassification:
            print 'FAILURE FOR:', iPiece, YExtraDataTest[iPiece][0]
        else:
            totalCorrect+=1
        classifications.append((YExtraDataTest[iPiece][0], classification==trueClassification))
    print classifications
    wholePieceAcc = float(totalCorrect)/len(xTestByPiece)
    print 'Overall whole-piece accuracy:', float(totalCorrect)/len(xTestByPiece)
    if wholePieceAcc == 1 or wholePieceAcc == 0:
        err = 'undefined'
    else:
        err = np.sqrt(wholePieceAcc*(1-wholePieceAcc)/len(xTestByPiece))
    print 'Error in whole-piece accuracy is', err
