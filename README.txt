Crab is a neural network trained to identify music it hasn't seen before by figuring out stylistic aspects of different composers. 

The code gets an accuracy of 95% classifying a training set of tracks it has never seen before as one of 3 different composers.

The training set is about 53 minutes of music (stored in MIDI format) by Bach, Beethoven and Chopin. The code parses the MIDI files (using the external library python-midi) and converts them into a matrix (see example.png) where x is time and y is pitch, with the magnitude of the matrix entries giving the volume of each note. Each track is sliced so that each such matrix corresponds to 10 seconds of music. The network is trained to classify these slices as one of the three composers in the set.

The application of this sort of network would be automated tagging of metadata - not just of composer/artist, but also of e.g. genre, beats per minute, etc.

To evaluate an entire track (rather than one 10-second slice), the track is split up into 10-second slices and the network's classification of each of them is averaged to form an overall prediction. This gives the accuracy figure quoted above. The accuracy on any individual 10-second slice is ?.

The network is a convolutional neural network implemented in Keras. CNNs are useful here because the translational invariances that make them good for spotting features in images are still valid - a piece of music is the same piece of music if transposed upwards or downwards (translation through pitch) or if there's a delay at the start (translation through time).

Visualising the filters by adjusting the input image so as to maximise their activation is quite interesting - conv2d_2_filter_13.png seems to pick out trills, a device in which the music alternates between 2 adjacent notes very rapidly. conv2d_1_filter_17.png seems to discriminate based on whether particularly low notes are used.

Depencencies:
python-midi (github.com/vishnubob/python-midi)
scikit-learn (scikit-learn.org)
keras (keras.io)

Neural network architecture (as output by Keras):
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
conv2d_1 (Conv2D)            (None, 200, 80, 32)       1568      
_________________________________________________________________
conv2d_2 (Conv2D)            (None, 100, 40, 32)       25632     
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 50, 20, 32)        0         
_________________________________________________________________
conv2d_3 (Conv2D)            (None, 50, 20, 32)        9248      
_________________________________________________________________
max_pooling2d_2 (MaxPooling2 (None, 25, 10, 32)        0         
_________________________________________________________________
conv2d_4 (Conv2D)            (None, 25, 10, 32)        9248      
_________________________________________________________________
max_pooling2d_3 (MaxPooling2 (None, 12, 5, 32)         0         
_________________________________________________________________
conv2d_5 (Conv2D)            (None, 12, 5, 64)         18496     
_________________________________________________________________
max_pooling2d_4 (MaxPooling2 (None, 6, 2, 64)          0         
_________________________________________________________________
flatten_1 (Flatten)          (None, 768)               0         
_________________________________________________________________
dense_1 (Dense)              (None, 100)               76900     
_________________________________________________________________
leaky_re_lu_1 (LeakyReLU)    (None, 100)               0         
_________________________________________________________________
dropout_1 (Dropout)          (None, 100)               0         
_________________________________________________________________
dense_2 (Dense)              (None, 50)                5050      
_________________________________________________________________
leaky_re_lu_2 (LeakyReLU)    (None, 50)                0         
_________________________________________________________________
dropout_2 (Dropout)          (None, 50)                0         
_________________________________________________________________
dense_3 (Dense)              (None, 3)                 153       
=================================================================
Total params: 146,295
Trainable params: 146,295
Non-trainable params: 0
