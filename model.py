from keras.models import Model, Sequential
from keras.layers import Input, Dense, Flatten, Convolution2D, MaxPooling2D, Dropout, Reshape, LeakyReLU
from keras import backend as K
import midi
#from keras.layers.normalization 

def get_model(nX, nY, nClasses):

    # Model parameters
    rows = nX
    cols = nY
    inputShape = (nX, nY)

    convSize = 4
    convSizeTime = 2
    convSizePitch = 24
    nConv = 32

    inp = Input(shape=inputShape)
    #flat = Flatten()(inp)

    #################
    # convSize x convSize convolutions
    #################

    model = Sequential()

    acti = 'linear'
    alpha=0.3

    model.add(Convolution2D(nConv, (convSizeTime, convSizePitch), strides=1, activation=acti, border_mode='same', 
              batch_input_shape=(None, rows, cols, 1)))
    #model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Convolution2D(nConv, 5, strides=2, activation=acti, border_mode='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Convolution2D(nConv, 3, strides=1, activation=acti, border_mode='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Convolution2D(nConv, 3, strides=1, activation=acti, border_mode='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Convolution2D(nConv*2, 3, strides=1, activation=acti, border_mode='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    #model.add(Convolution2D(nConv, convSize, convSize, activation='relu', border_mode='same'))
    #model.add(MaxPooling2D(pool_size=(2,2)))

    model.add(Flatten())

    #hidden_1 = Dense(hidden_size, activation='relu')(flat)

    hiddenSize=100
    model.add(Dense(hiddenSize, activation=acti))
    model.add(LeakyReLU(alpha=alpha))
    model.add(Dropout(0.5))
    model.add(Dense(hiddenSize/2, activation=acti))
    model.add(LeakyReLU(alpha=alpha))
    model.add(Dropout(0.5))
    #model.add(Dense(hiddenSize/4, activation=acti))
    #model.add(LeakyReLU(alpha=alpha))
    #model.add(Dropout(0.5))
    model.add(Dense(nClasses, activation='softmax'))

    #model = Model(input=inp, output=out)

    print(model.summary())

    return model


if __name__ == '__main__':

    model = get_model()
